create table if not exists author(
   id bigserial primary key,
   name varchar(255) unique not null
);

insert into author(name) values ('Stephen King');
insert into author(name) values ('Arthur Conan Doyle');
insert into author(name) values ('Joanne Rowling');