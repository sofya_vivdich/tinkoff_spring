create table if not exists genre(
    id bigserial primary key,
    name varchar(255) unique not null
);

insert into genre(name) values ('detective');
insert into genre(name) values ('fantasy');
