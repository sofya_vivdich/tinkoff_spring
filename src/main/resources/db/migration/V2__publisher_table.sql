create table if not exists publisher(
     id bigserial primary key,
     name varchar(255) not null,
     fk_address_id bigint unique references address(id)
);

insert into publisher(name, fk_address_id) values ('Publisher1', 1);
insert into publisher(name, fk_address_id) values ('Publisher2', 2);