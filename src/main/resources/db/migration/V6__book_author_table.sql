create table if not exists book_author(
   book_id bigint references book(id) on delete cascade on update cascade,
   author_id bigint references author(id) on delete cascade on update cascade,
   constraint book_author_pk primary key (book_id, author_id)
);

insert into book_author(book_id, author_id) values (1, 1);
insert into book_author(book_id, author_id) values (2, 2);
insert into book_author(book_id, author_id) values (3, 3);
insert into book_author(book_id, author_id) values (4, 3);