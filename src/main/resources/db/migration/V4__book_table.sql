create table if not exists book(
    id bigserial primary key,
    name varchar(255) not null,
    fk_genre_id bigint references genre(id) on delete set null,
    fk_publisher_id bigint references publisher(id) on delete set null,
    publication_date date,
    price numeric not null
);

insert into book(name, fk_genre_id, fk_publisher_id, price)                   values ('1408', 1, 1, 790.90);
insert into book(name, fk_genre_id, fk_publisher_id, publication_date, price) values ('The Adventures of Sherlock Holmes', 1, 2, date '1892-10-14', 1129.90);
insert into book(name, fk_genre_id, fk_publisher_id, price)                   values ('Harry Potter and the Philosophers Stone', 2, 1, 990.50);
insert into book(name, fk_genre_id, fk_publisher_id, price)                   values ('Harry Potter and the Goblet of Fire', 2, 1, 990.50)
