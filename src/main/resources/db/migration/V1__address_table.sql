create table if not exists address(
        id bigserial primary key,
        index varchar(6) not null,
        region varchar(100),
        town varchar(100) not null,
        street varchar(50) not null,
        house_number integer not null,
        building varchar(10)
);

insert into address(index, town, street, house_number) values ('356198', 'Moscow', 'Lenin Street', 44);
insert into address(index, town, street, house_number) values ('653189', 'Saint Petersburg', 'Admiralteysky prospect', 167);
