package ru.tinkoff.library.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ValidationFailedHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationFailed(MethodArgumentNotValidException ex) {
        Map<String, String> fieldsWithError = new HashMap<>();

        ex.getBindingResult().getFieldErrors().forEach(error -> {
            fieldsWithError.put(error.getField(), error.getDefaultMessage());
        });

        return fieldsWithError;
    }
}
