package ru.tinkoff.library.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublisherController {
    private final PublisherService publisherService;

    @GetMapping("/{publisherId}")
    public Publisher getById(@PathVariable Long publisherId) {
        return publisherService.getById(publisherId);
    }

    @GetMapping
    public List<Publisher> getAll() {
        return publisherService.getAll();
    }

    @PostMapping
    public Publisher save(@RequestBody @Valid CreatingPublisherDto publisherDto) {
        return publisherService.save(publisherDto);
    }

    @PutMapping("/{publisherId}")
    public Publisher update(@PathVariable Long publisherId, @RequestBody @Valid CreatingPublisherDto publisherDto) {
        return publisherService.update(publisherId, publisherDto);
    }

    @DeleteMapping("/{publisherId}")
    public void deleteById(@PathVariable Long publisherId) {
        publisherService.deleteById(publisherId);
    }

    @DeleteMapping
    public void deleteAll() {
        publisherService.deleteAll();
    }
}
