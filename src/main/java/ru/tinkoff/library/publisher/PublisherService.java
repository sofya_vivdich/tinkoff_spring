package ru.tinkoff.library.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.library.address.Address;
import ru.tinkoff.library.address.AddressService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PublisherService {
    private final PublisherRepository publisherRepository;
    private final AddressService addressService;

    public Publisher getById(Long publisherId) {
        return publisherRepository.findById(publisherId)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find publisher with id = " + publisherId));
    }

    public List<Publisher> getAll() {
        return publisherRepository.findAll();
    }

    public Publisher save(CreatingPublisherDto publisherDto) {
        Address address = addressService.save(publisherDto.getAddressDto());

        return publisherRepository.save(Publisher.builder()
                .id(address.getId())
                .name(publisherDto.getName())
                .address(address)
                .build());
    }

    public Publisher update(Long publisherId, CreatingPublisherDto publisherDto) {
        Address address = addressService.update(publisherId, publisherDto.getAddressDto());

        Publisher publisherForUpdating = getById(publisherId);
        publisherForUpdating.setName(publisherDto.getName());
        publisherForUpdating.setAddress(address);

        return publisherRepository.save(publisherForUpdating);
    }

    public void deleteById(Long publisherId) {
        publisherRepository.deleteById(publisherId);
    }

    public void deleteAll() {
        publisherRepository.deleteAll();
    }

    public boolean notExistsById(Long publisherId) {
        return !publisherRepository.existsById(publisherId);
    }
}
