package ru.tinkoff.library.publisher;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.library.address.CreatingAddressDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingPublisherDto {
    @NotBlank(message = "Publisher's name can't be empty")
    private String name;

    @Valid
    private CreatingAddressDto addressDto;
}
