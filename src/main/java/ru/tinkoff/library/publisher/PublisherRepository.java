package ru.tinkoff.library.publisher;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
    @EntityGraph(value = "Publisher.address")
    Optional<Publisher> findById(Long id);

    @EntityGraph(value = "Publisher.address")
    List<Publisher> findAll();
}
