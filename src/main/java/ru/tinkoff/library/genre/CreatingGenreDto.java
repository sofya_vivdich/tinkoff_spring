package ru.tinkoff.library.genre;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingGenreDto {
    @NotBlank(message = "Genre's name can't be empty")
    private String name;
}
