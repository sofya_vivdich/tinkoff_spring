package ru.tinkoff.library.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GenreService {
    private final GenreRepository genreRepository;

    public Genre getById(Long genreId) {
        return genreRepository.findById(genreId)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find genre with id = " + genreId));
    }

    public Genre getByName(String name) {
        return genreRepository.findByNameEquals(name)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find genre with name = " + name));
    }

    public List<Genre> getAll() {
        return genreRepository.findAll();
    }

    public Genre save(CreatingGenreDto genreDto) {
        Genre genreForSaving = Genre.builder()
                .name(genreDto.getName())
                .build();

        return genreRepository.save(genreForSaving);
    }

    public Genre update(Long genreId, CreatingGenreDto genreDto) {
        Genre genreForUpdating = getById(genreId);
        genreForUpdating.setName(genreDto.getName());

        return genreRepository.save(genreForUpdating);
    }

    public void deleteById(Long genreId) {
        genreRepository.deleteById(genreId);
    }

    public void deleteAll() {
        genreRepository.deleteAll();
    }

    public boolean notExistsById(Long genreId) {
        return !genreRepository.existsById(genreId);
    }
}
