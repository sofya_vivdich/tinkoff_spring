package ru.tinkoff.library.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {
    private final GenreService genreService;

    @GetMapping("/{genreId}")
    public Genre getById(@PathVariable Long genreId) {
        return genreService.getById(genreId);
    }

    @GetMapping
    public List<Genre> getAll() {
        return genreService.getAll();
    }

    @PostMapping
    public Genre save(@RequestBody @Valid CreatingGenreDto genreDto) {
        return genreService.save(genreDto);
    }

    @PutMapping("/{genreId}")
    public Genre update(@PathVariable Long genreId, @RequestBody @Valid CreatingGenreDto genreDto) {
        return genreService.update(genreId, genreDto);
    }

    @DeleteMapping("/{genreId}")
    public void deleteById(@PathVariable Long genreId) {
        genreService.deleteById(genreId);
    }

    @DeleteMapping
    public void deleteAll() {
        genreService.deleteAll();
    }
}