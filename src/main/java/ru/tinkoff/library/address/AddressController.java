package ru.tinkoff.library.address;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/addresses")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @GetMapping("/{addressId}")
    public Address getById(@PathVariable Long addressId) {
        return addressService.getById(addressId);
    }

    @GetMapping
    public List<Address> getAll() {
        return addressService.getAll();
    }

    @PutMapping("/{addressId}")
    public Address update(@PathVariable Long addressId, @RequestBody @Valid CreatingAddressDto addressDto) {
        return addressService.update(addressId, addressDto);
    }

    @DeleteMapping("/{addressId}")
    public void delete(@PathVariable Long addressId) {
        addressService.deleteById(addressId);
    }

    @DeleteMapping
    public void deleteAll() {
        addressService.deleteAll();
    }
}
