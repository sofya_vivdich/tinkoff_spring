package ru.tinkoff.library.address;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    @EntityGraph(value = "Address.publisher")
    Optional<Address> findById(Long id);

    @EntityGraph(value = "Address.publisher")
    List<Address> findAll();
}
