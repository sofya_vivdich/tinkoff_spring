package ru.tinkoff.library.address;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;

    public Address getById(Long addressId) {
        return addressRepository.findById(addressId)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find address with id = " + addressId));
    }

    public List<Address> getAll() {
        return addressRepository.findAll();
    }

    public Address save(CreatingAddressDto addressDto) {
        return addressRepository.save(Address.builder()
                .index(addressDto.getIndex())
                .region(addressDto.getRegion())
                .town(addressDto.getTown())
                .street(addressDto.getStreet())
                .houseNumber(addressDto.getHouseNumber())
                .building(addressDto.getBuilding())
                .build());
    }

    public Address update(Long addressId, CreatingAddressDto addressDto) {
        Address addressForUpdating = getById(addressId);

        addressForUpdating.setIndex(addressDto.getIndex());
        addressForUpdating.setRegion(addressDto.getRegion());
        addressForUpdating.setTown(addressDto.getTown());
        addressForUpdating.setStreet(addressDto.getStreet());
        addressForUpdating.setHouseNumber(addressDto.getHouseNumber());
        addressForUpdating.setBuilding(addressDto.getBuilding());

        return addressRepository.save(addressForUpdating);
    }

    public void deleteById(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    public void deleteAll() {
        addressRepository.deleteAll();
    }
}
