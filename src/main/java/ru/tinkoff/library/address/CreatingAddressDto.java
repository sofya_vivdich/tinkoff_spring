package ru.tinkoff.library.address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingAddressDto {
    @NotBlank(message = "index can't be empty")
    private String index;

    private String region;

    @NotBlank(message = "town can't be empty")
    private String town;

    @NotBlank(message = "street can't be empty")
    private String street;

    @NotNull(message = "houseNumber can't be null")
    @Min(value = 1, message = "houseNumber must be at least 1")
    private Integer houseNumber;

    private String building;
}


