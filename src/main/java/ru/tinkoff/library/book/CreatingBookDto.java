package ru.tinkoff.library.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingBookDto {
    @NotBlank(message = "Book's name can't be empty")
    private String name;

    @NotNull(message = "genreId can't be null")
    private Long genreId;

    @NotNull(message = "publisherId can't be null")
    private Long publisherId;

    private LocalDate publicationDate;

    private List<Long> authorsId;

    @NotNull(message = "price can't be null")
    @Min(value = 0, message = "price must be at least 0")
    private BigDecimal price;
}
