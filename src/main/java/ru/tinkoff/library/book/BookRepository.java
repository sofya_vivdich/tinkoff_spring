package ru.tinkoff.library.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {
    @EntityGraph(value = "Book.authors")
    Optional<Book> findById(Long id);

    @EntityGraph(value = "Book.authors")
    List<Book> findAll();

    @EntityGraph(value = "Book.authors")
    List<Book> findAll(Specification<Book> specification);

    @EntityGraph(value = "Book.authors")
    Page<Book> findAll(Pageable pageable);

    @EntityGraph(value = "Book.authors")
    List<Book> findAll(Sort sort);
}
