package ru.tinkoff.library.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.tinkoff.library.author.Author;
import ru.tinkoff.library.author.AuthorService;
import ru.tinkoff.library.genre.Genre;
import ru.tinkoff.library.genre.GenreService;
import ru.tinkoff.library.publisher.Publisher;
import ru.tinkoff.library.publisher.PublisherService;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    private final GenreService genreService;
    private final PublisherService publisherService;
    private final AuthorService authorService;

    private static final LocalDate DATE_START_21_CENTURY = LocalDate.of(1999, 12, 31);
    private static final String MIDDLE_PRICE = "1000";

    public Book getById(Long bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find book with id = " + bookId));
    }

    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    public List<Book> getAllWithSorting(String field) {
        return bookRepository.findAll(Sort.by(field));
    }

    public List<Book> getPageOfBooks(int page, int size) {
        return bookRepository.findAll(PageRequest.of(page, size)).getContent();
    }

    public List<Book> getSortPageOfBooks(int pageNumber, int pageSize, String field) {
        return bookRepository.findAll(PageRequest.of(pageNumber, pageSize, Sort.by(field))).getContent();
    }

    public List<Book> getModernBooksWithPriceLessThan(String priceStr) {
        Specification<Book> specification = Specification
                .where(BookSpecifications.booksAfterDate(DATE_START_21_CENTURY)
                        .and(BookSpecifications.booksByPriceLessThan(new BigDecimal(priceStr))));
        return bookRepository.findAll(specification);
    }

    public List<Book> getByGenreNameMiddlePrice(String genreName) {
        Specification<Book> specification = Specification
                .where(BookSpecifications.booksByGenre(genreService.getByName(genreName)))
                .and(BookSpecifications.booksByPriceLessThan(new BigDecimal(MIDDLE_PRICE)));
        return bookRepository.findAll(specification);
    }

    public List<Book> getByAuthorNameAndGenreName(String authorName, String genreName) {
        Specification<Book> specification = Specification
                .where(BookSpecifications.booksByAuthor(authorService.getByName(authorName)))
                .and(BookSpecifications.booksByGenre(genreService.getByName(genreName)));
        return bookRepository.findAll(specification);
    }

    public List<Book> getByNamePattern(String pattern) {
        Specification<Book> specification = Specification.where(BookSpecifications.booksByNamePattern(pattern));
        return bookRepository.findAll(specification);
    }

    public Book save(CreatingBookDto bookDto) {
        if (publisherService.notExistsById(bookDto.getPublisherId())) {
            throw new IllegalArgumentException("Can't create book with non existing publisherId = " + bookDto.getPublisherId());
        }

        if (genreService.notExistsById(bookDto.getGenreId())) {
            throw new IllegalArgumentException("Can't create book with non existing genreId = " + bookDto.getGenreId());
        }

        List<Author> authors = bookDto.getAuthorsId() == null ? null :
                new ArrayList<>(bookDto.getAuthorsId().stream().map(authorService::getById).toList());

        return bookRepository.save(Book.builder()
                .name(bookDto.getName())
                .genre(genreService.getById(bookDto.getGenreId()))
                .publisher(publisherService.getById(bookDto.getPublisherId()))
                .publicationDate(bookDto.getPublicationDate())
                .authors(authors)
                .price(bookDto.getPrice())
                .build());
    }

    public Book update(Long bookId, CreatingBookDto bookDto) {
        Book book = getById(bookId);

        List<Author> authors = bookDto.getAuthorsId() == null ? null :
                new ArrayList<>(bookDto.getAuthorsId().stream().map(authorService::getById).toList());

        book.setName(bookDto.getName());
        Genre genre = genreService.getById(bookDto.getGenreId());
        book.setGenre(genre);
        Publisher publisher = publisherService.getById(bookDto.getPublisherId());
        book.setPublisher(publisher);
        book.setPublicationDate(bookDto.getPublicationDate());
        book.setAuthors(authors);
        book.setPrice(bookDto.getPrice());

        return bookRepository.save(book);
    }

    public void deleteById(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    public void deleteAll() {
        bookRepository.deleteAll();
    }
}
