package ru.tinkoff.library.book;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping("/{bookId}")
    public Book getById(@PathVariable Long bookId) {
        return bookService.getById(bookId);
    }

    @GetMapping
    public List<Book> getAll() {
        return bookService.getAll();
    }

    @GetMapping("/search/modern")
    public List<Book> getModernBooksWithPriceLessThan(@RequestParam(name = "priceLessThan") String priceStr) {
        return bookService.getModernBooksWithPriceLessThan(priceStr);
    }

    @GetMapping("/search/middlePrice/byGenreName")
    public List<Book> getByGenreNameMiddlePrice(@RequestParam(name = "genreName") String genreName) {
        return bookService.getByGenreNameMiddlePrice(genreName);
    }

    @GetMapping("/search/byAuthorAndGenre")
    public List<Book> getByAuthorNameAndGenreName(
            @RequestParam(name = "authorName") String authorName,
            @RequestParam(name = "genreName") String genreName) {
        return bookService.getByAuthorNameAndGenreName(authorName, genreName);
    }

    @GetMapping("/search/byNamePattern")
    public List<Book> getByNamePattern(@RequestParam(name = "pattern") String pattern) {
        return bookService.getByNamePattern(pattern);
    }

    @GetMapping("/sort/{field}")
    public List<Book> getAllWithSorting(@PathVariable String field) {
        return bookService.getAllWithSorting(field);
    }

    @GetMapping("/pages")
    public List<Book> getPageOfBooks(
            @RequestParam(name = "pageNumber") int pageNumber,
            @RequestParam(name = "pageSize") int pageSize) {
        return bookService.getPageOfBooks(pageNumber, pageSize);
    }

    @GetMapping("/sortPages/{field}")
    public List<Book> getSortPageOfBooks(
            @PathVariable String field,
            @RequestParam(name = "pageNumber") int pageNumber,
            @RequestParam(name = "pageSize") int pageSize) {
        return bookService.getSortPageOfBooks(pageNumber, pageSize, field);
    }

    @PostMapping
    public Book save(@RequestBody @Valid CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    @PutMapping("/{bookId}")
    public Book update(@PathVariable Long bookId, @RequestBody @Valid CreatingBookDto bookDto) {
        return bookService.update(bookId, bookDto);
    }

    @DeleteMapping("/{bookId}")
    public void deleteById(@PathVariable Long bookId) {
        bookService.deleteById(bookId);
    }

    @DeleteMapping
    public void deleteAll() {
        bookService.deleteAll();
    }
}
