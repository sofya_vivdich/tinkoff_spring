package ru.tinkoff.library.book;

import org.springframework.data.jpa.domain.Specification;
import ru.tinkoff.library.author.Author;
import ru.tinkoff.library.genre.Genre;

import java.math.BigDecimal;
import java.time.LocalDate;

public class BookSpecifications {
    public static Specification<Book> booksByNamePattern(String pattern) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("name"), "%" + pattern + "%"));
    }

    public static Specification<Book> booksAfterDate(LocalDate date) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("publicationDate"), date));
    }

    public static Specification<Book> booksByPriceLessThan(BigDecimal price) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("price"), price));
    }

    public static Specification<Book> booksByGenre(Genre genre) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("genre"), genre));
    }

    public static Specification<Book> booksByAuthor(Author author) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isMember(author, root.get("authors")));
    }
}
