package ru.tinkoff.library.author;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingAuthorDto {
    @NotBlank(message = "Author's name can't be empty")
    private String name;
}
