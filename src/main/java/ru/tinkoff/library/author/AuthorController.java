package ru.tinkoff.library.author;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping("/{authorId}")
    public Author getById(@PathVariable Long authorId) {
        return authorService.getById(authorId);
    }

    @GetMapping
    public List<Author> getAll() {
        return authorService.getAll();
    }

    @PostMapping
    public Author save(@RequestBody @Valid CreatingAuthorDto authorDto) {
        return authorService.save(authorDto);
    }

    @PutMapping("/{authorId}")
    public Author update(@PathVariable Long authorId, @RequestBody @Valid CreatingAuthorDto authorDto) {
        return authorService.update(authorId, authorDto);
    }

    @DeleteMapping("/{authorId}")
    public void delete(@PathVariable Long authorId) {
        authorService.deleteById(authorId);
    }

    @DeleteMapping
    public void deleteAll() {
        authorService.deleteAll();
    }
}
