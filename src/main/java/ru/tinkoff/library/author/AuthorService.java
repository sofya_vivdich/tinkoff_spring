package ru.tinkoff.library.author;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;

    public Author getById(Long authorId) {
        return authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find author with id = " + authorId));
    }

    public List<Author> getAll() {
        return authorRepository.findAll();
    }

    public Author getByName(String name) {
        return authorRepository.findByNameEquals(name)
                .orElseThrow(() -> new EntityNotFoundException("Doesn't find author with name = " + name));
    }

    public Author save(CreatingAuthorDto authorDto) {
        Author authorForSaving = Author.builder()
                .name(authorDto.getName())
                .build();

        return authorRepository.save(authorForSaving);
    }

    public Author update(Long authorId, CreatingAuthorDto authorDto) {
        Author author = getById(authorId);
        author.setName(authorDto.getName());

        return authorRepository.save(author);
    }

    public void deleteById(Long authorId) {
        authorRepository.deleteById(authorId);
    }

    public void deleteAll() {
        authorRepository.deleteAll();
    }
}
