package ru.tinkoff.library;

import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import java.time.Duration;

public class Containers implements BeforeAllCallback, ExtensionContext.Store.CloseableResource {
    public static final String POSTGRE_USERNAME = "postgres";
    public static final String POSTGRE_DATABASE = "postgres";

    private static final Network network = Network.newNetwork();

    public static Flyway flyway;

    public static PostgreSQLContainer postgre = new PostgreSQLContainer<>("postgres:latest")
            .withNetwork(network)
            .withNetworkAliases("postgre")
            .withDatabaseName(POSTGRE_DATABASE)
            .withUsername(POSTGRE_USERNAME)
            .withExposedPorts(5432)
            .withStartupTimeout(Duration.ofMinutes(10))
            .withLogConsumer(new Slf4jLogConsumer(LoggerFactory.getLogger(PostgreSQLContainer.class)));

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        if (extensionContext.getRoot().getStore(ExtensionContext.Namespace.GLOBAL).get("Containers") == null) {
            network.getId();

            postgre.start();
            migrateSchemaWithPrimaryData();

            extensionContext.getRoot().getStore(ExtensionContext.Namespace.GLOBAL).put("Containers", "started");
        }
    }

    @Override
    public void close() {
        postgre.stop();
        network.close();
    }

    private static void migrateSchemaWithPrimaryData() {
        flyway = Flyway.configure()
                .locations("classpath:db/migration")
                .dataSource(postgre.getJdbcUrl(), POSTGRE_USERNAME, postgre.getPassword()).load();

        flyway.migrate();
    }
}