package ru.tinkoff.library.spring_integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.tinkoff.library.address.Address;
import ru.tinkoff.library.address.AddressRepository;
import ru.tinkoff.library.address.CreatingAddressDto;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.ADDRESS1;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.ADDRESS2;

@AutoConfigureMockMvc
public class AddressControllerTest extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private Flyway flyway;

    @AfterEach
    public void returnDbToInitialState() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getById() throws Exception {
        Long addressId = 1L;

        String expectedContent = objectMapper.writeValueAsString(ADDRESS1);

        mockMvc.perform(MockMvcRequestBuilders.get("/addresses/{addressId}", addressId))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByIdDoesNotExist() throws Exception {
        Long addressId = 100L;

        this.mockMvc.perform(MockMvcRequestBuilders.get("/addresses/{addressId}", addressId))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(x -> assertEquals("Doesn't find address with id = 100", x.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Address> expectedAddressesList = List.of(
                ADDRESS1,
                ADDRESS2
        );

        String expectedContent = objectMapper.writeValueAsString(expectedAddressesList);
        mockMvc.perform(MockMvcRequestBuilders.get("/addresses"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void updateSuccessfully() throws Exception {
        Long addressId = 1L;

        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("123456")
                .town("Saint Petersburg")
                .street("Some Street")
                .houseNumber(12)
                .build();

        Address expectedAddress = Address.builder()
                .id(addressId)
                .index(addressDto.getIndex())
                .region(addressDto.getRegion())
                .town(addressDto.getTown())
                .street(addressDto.getStreet())
                .houseNumber(addressDto.getHouseNumber())
                .building(addressDto.getBuilding())
                .build();

        String content = objectMapper.writeValueAsString(addressDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/addresses/{addressId}", addressId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedAddress, addressRepository.findById(addressId).get()));
    }

    @Test
    public void updateValidationFailedException() throws Exception {
        Long addressId = 1L;

        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("")
                .town("")
                .street("")
                .houseNumber(-234)
                .build();

        String content = objectMapper.writeValueAsString(addressDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/addresses/{addressId}", addressId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.index", is("index can't be empty")))
                .andExpect(jsonPath("$.town", is("town can't be empty")))
                .andExpect(jsonPath("$.street", is("street can't be empty")))
                .andExpect(jsonPath("$.houseNumber", is("houseNumber must be at least 1")));
    }

    @Test
    public void deleteById() throws Exception {
        Long addressId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/addresses/{addressId}", addressId))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(addressRepository.existsById(addressId)));
    }

    @Test
    public void deleteAll() throws Exception {
        int expectedCountAddresses = 0;

        mockMvc.perform(MockMvcRequestBuilders.delete("/addresses"))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCountAddresses, addressRepository.findAll().size()));
    }
}
