package ru.tinkoff.library.spring_integration.constants;

import ru.tinkoff.library.book.Book;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_DETECTIVE;
import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_FANTASY;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.PUBLISHER1;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.PUBLISHER2;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.STEPHEN_KING;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.ARTHUR_CONAN_DOYLE;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.JOANNE_ROWLING;

public class BookConstants {

    public static final Book BOOK_1408 = Book.builder()
            .id(1L)
            .name("1408")
            .authors(List.of(STEPHEN_KING))
            .genre(GENRE_DETECTIVE)
            .publisher(PUBLISHER1)
            .price(new BigDecimal("790.90"))
            .build();

    public static final Book BOOK_SHERLOCK_HOLMES = Book.builder()
            .id(2L)
            .name("The Adventures of Sherlock Holmes")
            .authors(List.of(ARTHUR_CONAN_DOYLE))
            .genre(GENRE_DETECTIVE)
            .publisher(PUBLISHER2)
            .publicationDate(LocalDate.of(1892, 10, 14))
            .price(new BigDecimal("1129.90"))
            .build();

    public static final Book BOOK_HARRY_POTTER_STONE = Book.builder()
            .id(3L)
            .name("Harry Potter and the Philosophers Stone")
            .authors(List.of(JOANNE_ROWLING))
            .genre(GENRE_FANTASY)
            .publisher(PUBLISHER1)
            .price(new BigDecimal("990.50"))
            .build();

    public static final Book BOOK_HARRY_POTTER_GOBLET = Book.builder()
            .id(4L)
            .name("Harry Potter and the Goblet of Fire")
            .authors(List.of(JOANNE_ROWLING))
            .genre(GENRE_FANTASY)
            .publisher(PUBLISHER1)
            .price(new BigDecimal("990.50"))
            .build();
}
