package ru.tinkoff.library.spring_integration.constants;

import ru.tinkoff.library.genre.Genre;

public class GenreConstants {
    public static final Genre GENRE_DETECTIVE = Genre.builder()
            .id(1L)
            .name("detective")
            .build();

    public static final Genre GENRE_FANTASY = Genre.builder()
            .id(2L)
            .name("fantasy")
            .build();
}
