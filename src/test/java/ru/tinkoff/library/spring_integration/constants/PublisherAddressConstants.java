package ru.tinkoff.library.spring_integration.constants;

import ru.tinkoff.library.address.Address;
import ru.tinkoff.library.publisher.Publisher;

public class PublisherAddressConstants {
    public static final Address ADDRESS1 = Address.builder()
            .id(1L)
            .index("356198")
            .town("Moscow")
            .street("Lenin Street")
            .houseNumber(44)
            .build();

    public static final Address ADDRESS2 = Address.builder()
            .id(2L)
            .index("653189")
            .town("Saint Petersburg")
            .street("Admiralteysky prospect")
            .houseNumber(167)
            .build();

    public static final Publisher PUBLISHER1 = Publisher.builder()
            .id(1L)
            .name("Publisher1")
            .address(ADDRESS1)
            .build();

    public static final Publisher PUBLISHER2 = Publisher.builder()
            .id(2L)
            .name("Publisher2")
            .address(ADDRESS2)
            .build();
}
