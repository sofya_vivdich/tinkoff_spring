package ru.tinkoff.library.spring_integration.constants;

import ru.tinkoff.library.author.Author;

public class AuthorConstants {
    public static final Author STEPHEN_KING = Author.builder()
            .id(1L)
            .name("Stephen King")
            .build();

    public static final Author ARTHUR_CONAN_DOYLE = Author.builder()
            .id(2L)
            .name("Arthur Conan Doyle")
            .build();

    public static final Author JOANNE_ROWLING = Author.builder()
            .id(3L)
            .name("Joanne Rowling")
            .build();
}
