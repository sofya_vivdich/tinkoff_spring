package ru.tinkoff.library.spring_integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.tinkoff.library.author.Author;
import ru.tinkoff.library.author.AuthorRepository;
import ru.tinkoff.library.author.CreatingAuthorDto;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.ARTHUR_CONAN_DOYLE;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.JOANNE_ROWLING;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.STEPHEN_KING;

@AutoConfigureMockMvc
public class AuthorControllerTest extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private Flyway flyway;

    @AfterEach
    public void returnDbToInitialState() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getById() throws Exception {
        Long authorId = 1L;

        String expectedContent = objectMapper.writeValueAsString(STEPHEN_KING);

        mockMvc.perform(MockMvcRequestBuilders.get("/authors/{authorId}", authorId))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByIdDoesNotExist() throws Exception {
        Long authorId = 100L;

        this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/{authorId}", authorId))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(x -> assertEquals("Doesn't find author with id = 100", x.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Author> expectedAuthorList = List.of(
                STEPHEN_KING,
                ARTHUR_CONAN_DOYLE,
                JOANNE_ROWLING
        );

        String expectedContent = objectMapper.writeValueAsString(expectedAuthorList);

        mockMvc.perform(MockMvcRequestBuilders.get("/authors"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void saveSuccessfully() throws Exception {
        Long authorId = 4L;

        CreatingAuthorDto authorDto = CreatingAuthorDto.builder()
                .name("Daniel Defoe")
                .build();

        Author expectedAuthor = Author.builder()
                .id(authorId)
                .name(authorDto.getName())
                .build();

        String content = objectMapper.writeValueAsString(authorDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/authors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedAuthor, authorRepository.findById(authorId).get()));
    }

    @Test
    public void saveValidationFailedException() throws Exception {
        CreatingAuthorDto authorDto = CreatingAuthorDto.builder()
                .name("")
                .build();

        String content = objectMapper.writeValueAsString(authorDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/authors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Author's name can't be empty")));
    }

    @Test
    public void updateSuccessfully() throws Exception {
        Long authorId = 1L;

        CreatingAuthorDto authorDto = CreatingAuthorDto.builder()
                .name("Stephen King renamed")
                .build();

        Author expectedAuthor = Author.builder()
                .id(authorId)
                .name(authorDto.getName())
                .build();

        String content = objectMapper.writeValueAsString(authorDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/authors/{authorId}", authorId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedAuthor, authorRepository.findById(authorId).get()));
    }

    @Test
    public void updateValidationFailedException() throws Exception {
        Long authorId = 1L;

        CreatingAuthorDto authorDto = CreatingAuthorDto.builder()
                .name("")
                .build();

        String content = objectMapper.writeValueAsString(authorDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/authors/{authorId}", authorId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Author's name can't be empty")));
    }

    @Test
    public void deleteById() throws Exception {
        Long authorId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/authors/{authorId}", authorId))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(authorRepository.existsById(authorId)));
    }

    @Test
    public void deleteAll() throws Exception {
        int expectedCountAuthors = 0;

        mockMvc.perform(MockMvcRequestBuilders.delete("/authors"))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCountAuthors, authorRepository.findAll().size()));
    }
}
