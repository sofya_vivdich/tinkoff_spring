package ru.tinkoff.library.spring_integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.tinkoff.library.book.Book;
import ru.tinkoff.library.book.BookRepository;
import ru.tinkoff.library.book.BookSpecifications;
import ru.tinkoff.library.book.CreatingBookDto;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.ARTHUR_CONAN_DOYLE;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.JOANNE_ROWLING;
import static ru.tinkoff.library.spring_integration.constants.AuthorConstants.STEPHEN_KING;
import static ru.tinkoff.library.spring_integration.constants.BookConstants.BOOK_1408;
import static ru.tinkoff.library.spring_integration.constants.BookConstants.BOOK_HARRY_POTTER_GOBLET;
import static ru.tinkoff.library.spring_integration.constants.BookConstants.BOOK_HARRY_POTTER_STONE;
import static ru.tinkoff.library.spring_integration.constants.BookConstants.BOOK_SHERLOCK_HOLMES;
import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_DETECTIVE;
import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_FANTASY;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.PUBLISHER2;

@AutoConfigureMockMvc
public class BookControllerTest extends AbstractIntegrationTest {

    private static final LocalDate DATE_START_21_CENTURY = LocalDate.of(1999, 12, 31);
    private static final BigDecimal MIDDLE_PRICE = new BigDecimal("1000");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private Flyway flyway;

    @AfterEach
    public void returnDbToInitialState() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getById() throws Exception {
        Long bookId = 1L;

        String expectedContent = new ObjectMapper().writeValueAsString(BOOK_1408);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/books/{bookId}", bookId))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByIdDoesNotExist() throws Exception {
        Long bookId = 100L;

        this.mockMvc.perform(MockMvcRequestBuilders.get("/books/{bookId}", bookId))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(x -> assertEquals("Doesn't find book with id = 100", x.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Book> expectedBooksList = List.of(
                BOOK_1408,
                BOOK_SHERLOCK_HOLMES,
                BOOK_HARRY_POTTER_STONE,
                BOOK_HARRY_POTTER_GOBLET
        );

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        mockMvc.perform(MockMvcRequestBuilders.get("/books"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void saveSuccessfully() throws Exception {
        Long bookId = 5L;

        CreatingBookDto bookDto = CreatingBookDto.builder()
                .name("new_book")
                .genreId(1L)
                .publisherId(2L)
                .authorsId(List.of(1L, 3L))
                .price(new BigDecimal("1000"))
                .build();

        Book expectedBook = Book.builder()
                .id(bookId)
                .name(bookDto.getName())
                .authors(List.of(
                        STEPHEN_KING,
                        JOANNE_ROWLING
                ))
                .genre(GENRE_DETECTIVE)
                .publisher(PUBLISHER2)
                .price(bookDto.getPrice())
                .build();

        String content = objectMapper.writeValueAsString(bookDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBook, bookRepository.findById(bookId).get()));
    }

    @Test
    public void saveValidationFailedException() throws Exception {
        CreatingBookDto bookDto = CreatingBookDto.builder()
                .name("")
                .genreId(null)
                .publisherId(null)
                .authorsId(List.of(1L))
                .price(new BigDecimal("-1000"))
                .build();

        String content = objectMapper.writeValueAsString(bookDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Book's name can't be empty")))
                .andExpect(jsonPath("$.genreId", is("genreId can't be null")))
                .andExpect(jsonPath("$.publisherId", is("publisherId can't be null")))
                .andExpect(jsonPath("$.price", is("price must be at least 0")));
    }

    @Test
    public void saveWithIllegalArgument() throws Exception {
        CreatingBookDto bookDto = CreatingBookDto.builder()
                .name("new_book")
                .genreId(3000L)
                .publisherId(2L)
                .authorsId(List.of(1L))
                .price(new BigDecimal("1000"))
                .build();

        String content = objectMapper.writeValueAsString(bookDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof IllegalArgumentException))
                .andExpect(x -> assertEquals("Can't create book with non existing genreId = 3000", x.getResolvedException().getMessage()));
    }

    @Test
    public void updateSuccessfully() throws Exception {
        Long bookId = 1L;

        CreatingBookDto bookDto = CreatingBookDto.builder()
                .name("book renamed")
                .genreId(2L)
                .publisherId(2L)
                .authorsId(List.of(2L))
                .price(new BigDecimal("590"))
                .build();

        Book expectedBook = Book.builder()
                .id(bookId)
                .name(bookDto.getName())
                .authors(List.of(ARTHUR_CONAN_DOYLE))
                .genre(GENRE_FANTASY)
                .publisher(PUBLISHER2)
                .price(bookDto.getPrice())
                .build();

        String content = objectMapper.writeValueAsString(bookDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/books/{bookId}", bookId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBook, bookRepository.findById(bookId).get()));
    }

    @Test
    public void updateValidationFailedException() throws Exception {
        Long bookId = 1L;

        CreatingBookDto bookDto = CreatingBookDto.builder()
                .name("")
                .genreId(null)
                .publisherId(null)
                .authorsId(List.of(2L))
                .price(new BigDecimal("-600"))
                .build();

        String content = objectMapper.writeValueAsString(bookDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/books/{bookId}", bookId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Book's name can't be empty")))
                .andExpect(jsonPath("$.genreId", is("genreId can't be null")))
                .andExpect(jsonPath("$.publisherId", is("publisherId can't be null")))
                .andExpect(jsonPath("$.price", is("price must be at least 0")));
    }

    @Test
    public void deleteById() throws Exception {
        Long bookId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/books/{bookId}", bookId))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(bookRepository.existsById(bookId)));
    }

    @Test
    public void deleteAll() throws Exception {
        int expectedCountBooks = 0;

        mockMvc.perform(MockMvcRequestBuilders.delete("/books"))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCountBooks, bookRepository.findAll().size()));
    }

    @Test
    public void getModernBooksWithPriceLessThan() throws Exception {
        String priceStr = "1200";

        List<Book> expectedBooksList = new ArrayList<>();
        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        Specification<Book> specification = Specification
                .where(BookSpecifications.booksAfterDate(DATE_START_21_CENTURY)
                        .and(BookSpecifications.booksByPriceLessThan(new BigDecimal(priceStr))));

        mockMvc.perform(MockMvcRequestBuilders.get("/books/search/modern")
                        .param("priceLessThan", priceStr))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBooksList.size(), bookRepository.findAll(specification).size()))
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByGenreNameMiddlePrice() throws Exception {
        String genreName = "detective";

        List<Book> expectedBooksList = List.of(BOOK_1408);
        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        Specification<Book> specification = Specification
                .where(BookSpecifications.booksByGenre(GENRE_DETECTIVE)
                        .and(BookSpecifications.booksByPriceLessThan(MIDDLE_PRICE)));

        mockMvc.perform(MockMvcRequestBuilders.get("/books/search/middlePrice/byGenreName")
                        .param("genreName", genreName))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBooksList.size(), bookRepository.findAll(specification).size()))
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByAuthorNameAndGenreName() throws Exception {
        String authorName = "Joanne Rowling";
        String genreName = "fantasy";

        List<Book> expectedBooksList = List.of(
                BOOK_HARRY_POTTER_STONE,
                BOOK_HARRY_POTTER_GOBLET
        );

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        Specification<Book> specification = Specification
                .where(BookSpecifications.booksByAuthor(JOANNE_ROWLING)
                        .and(BookSpecifications.booksByGenre(GENRE_FANTASY)));

        mockMvc.perform(MockMvcRequestBuilders.get("/books/search/byAuthorAndGenre")
                        .param("authorName", authorName)
                        .param("genreName", genreName))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBooksList.size(), bookRepository.findAll(specification).size()))
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByNamePattern() throws Exception {
        String pattern = "Potter";

        List<Book> expectedBooksList = List.of(
                BOOK_HARRY_POTTER_STONE,
                BOOK_HARRY_POTTER_GOBLET
        );

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        Specification<Book> specification = Specification.where(BookSpecifications.booksByNamePattern(pattern));

        mockMvc.perform(MockMvcRequestBuilders.get("/books/search/byNamePattern")
                        .param("pattern", pattern))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedBooksList.size(), bookRepository.findAll(specification).size()))
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getAllWithSorting() throws Exception {
        String sortField = "name";

        List<Book> expectedBooksList = List.of(
                BOOK_1408,
                BOOK_HARRY_POTTER_GOBLET,
                BOOK_HARRY_POTTER_STONE,
                BOOK_SHERLOCK_HOLMES
        );

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        mockMvc.perform(MockMvcRequestBuilders.get("/books/sort/{field}", sortField))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getPageOfBooks() throws Exception {
        int pageNumber = 1;
        int pageSize = 3;

        List<Book> expectedBooksList = List.of(BOOK_HARRY_POTTER_GOBLET);

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        mockMvc.perform(MockMvcRequestBuilders.get("/books/pages")
                        .param("pageNumber", String.valueOf(pageNumber))
                        .param("pageSize", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getSortPageOfBooks() throws Exception {
        String sortField = "price";
        int pageNumber = 1;
        int pageSize = 2;

        List<Book> expectedBooksList = List.of(
                BOOK_HARRY_POTTER_GOBLET,
                BOOK_SHERLOCK_HOLMES
        );

        String expectedContent = objectMapper.writeValueAsString(expectedBooksList);

        mockMvc.perform(MockMvcRequestBuilders.get("/books/sortPages/{field}", sortField)
                        .param("pageNumber", String.valueOf(pageNumber))
                        .param("pageSize", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }
}

