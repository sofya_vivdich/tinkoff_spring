package ru.tinkoff.library.spring_integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.tinkoff.library.address.Address;
import ru.tinkoff.library.address.CreatingAddressDto;
import ru.tinkoff.library.publisher.CreatingPublisherDto;
import ru.tinkoff.library.publisher.Publisher;
import ru.tinkoff.library.publisher.PublisherRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.PUBLISHER1;
import static ru.tinkoff.library.spring_integration.constants.PublisherAddressConstants.PUBLISHER2;

@AutoConfigureMockMvc
public class PublisherControllerTest extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private Flyway flyway;

    @AfterEach
    public void returnDbToInitialState() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getById() throws Exception {
        Long publisherId = 1L;

        String expectedContent = this.objectMapper.writeValueAsString(PUBLISHER1);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/publishers/{publisherId}", publisherId))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByIdDoesNotExist() throws Exception {
        Long publisherId = 100L;

        this.mockMvc.perform(MockMvcRequestBuilders.get("/publishers/{publisherId}", publisherId))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(x -> assertEquals("Doesn't find publisher with id = 100", x.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Publisher> expectedPublishersList = List.of(
                PUBLISHER1,
                PUBLISHER2
        );

        String expectedContent = this.objectMapper.writeValueAsString(expectedPublishersList);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/publishers"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void saveSuccessfully() throws Exception {
        Long publisherId = 3L;

        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("123456")
                .town("Saint Petersburg")
                .street("Some Street")
                .houseNumber(12)
                .build();

        CreatingPublisherDto publisherDto = CreatingPublisherDto.builder()
                .name("Publisher3")
                .addressDto(addressDto)
                .build();

        Publisher expectedPublisher = Publisher.builder()
                .id(publisherId)
                .name(publisherDto.getName())
                .address(Address.builder()
                        .id(publisherId)
                        .index(addressDto.getIndex())
                        .region(addressDto.getRegion())
                        .town(addressDto.getTown())
                        .street(addressDto.getStreet())
                        .houseNumber(addressDto.getHouseNumber())
                        .building(addressDto.getBuilding())
                        .build())
                .build();

        String content = objectMapper.writeValueAsString(publisherDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/publishers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedPublisher, publisherRepository.findById(publisherId).get()));
    }

    @Test
    public void saveValidationFailedException() throws Exception {
        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("123456")
                .town("Saint Petersburg")
                .street("Some Street renamed")
                .houseNumber(-12)
                .build();

        CreatingPublisherDto publisherDto = CreatingPublisherDto.builder()
                .name("")
                .addressDto(addressDto)
                .build();

        String content = objectMapper.writeValueAsString(publisherDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/publishers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Publisher's name can't be empty")))
                .andExpect(jsonPath("$.['addressDto.houseNumber']", is("houseNumber must be at least 1")));
    }

    @Test
    public void updateSuccessfully() throws Exception {
        Long publisherId = 1L;

        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("123456")
                .town("Saint Petersburg")
                .street("Some Street renamed")
                .houseNumber(12)
                .build();

        CreatingPublisherDto publisherDto = CreatingPublisherDto.builder()
                .name("Publisher3 renamed")
                .addressDto(addressDto)
                .build();

        Publisher expectedPublisher = Publisher.builder()
                .id(publisherId)
                .name(publisherDto.getName())
                .address(Address.builder()
                        .id(publisherId)
                        .index(addressDto.getIndex())
                        .region(addressDto.getRegion())
                        .town(addressDto.getTown())
                        .street(addressDto.getStreet())
                        .houseNumber(addressDto.getHouseNumber())
                        .building(addressDto.getBuilding())
                        .build())
                .build();

        String content = objectMapper.writeValueAsString(publisherDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/publishers/{publisherId}", publisherId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedPublisher, publisherRepository.findById(publisherId).get()));
    }

    @Test
    public void updateValidationFailedException() throws Exception {
        Long publisherId = 1L;

        CreatingAddressDto addressDto = CreatingAddressDto.builder()
                .index("123456")
                .town("Saint Petersburg")
                .street("Some Street renamed")
                .houseNumber(12)
                .build();

        CreatingPublisherDto publisherDto = CreatingPublisherDto.builder()
                .name("")
                .addressDto(addressDto)
                .build();

        String content = objectMapper.writeValueAsString(publisherDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/publishers/{publisherId}", publisherId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Publisher's name can't be empty")));
    }

    @Test
    public void deleteById() throws Exception {
        Long publisherId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/publishers/{publisherId}", publisherId))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(publisherRepository.existsById(publisherId)));
    }

    @Test
    public void deleteAll() throws Exception {
        int expectedCountPublishers = 0;

        mockMvc.perform(MockMvcRequestBuilders.delete("/publishers"))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCountPublishers, publisherRepository.findAll().size()));
    }
}
