package ru.tinkoff.library.spring_integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.tinkoff.library.genre.CreatingGenreDto;
import ru.tinkoff.library.genre.Genre;
import ru.tinkoff.library.genre.GenreRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_DETECTIVE;
import static ru.tinkoff.library.spring_integration.constants.GenreConstants.GENRE_FANTASY;

@AutoConfigureMockMvc
public class GenreControllerTest extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private Flyway flyway;

    @AfterEach
    public void returnDbToInitialState() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getById() throws Exception {
        Long genreId = 1L;

        String expectedContent = this.objectMapper.writeValueAsString(GENRE_DETECTIVE);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/genres/{genreId}", genreId))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByIdDoesNotExist() throws Exception {
        Long genreId = 100L;

        this.mockMvc.perform(MockMvcRequestBuilders.get("/genres/{genreId}", genreId))
                .andExpect(status().isNotFound())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(x -> assertEquals("Doesn't find genre with id = 100", x.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Genre> expectedGenresList = List.of(
                GENRE_DETECTIVE,
                GENRE_FANTASY
        );

        String expectedContent = this.objectMapper.writeValueAsString(expectedGenresList);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/genres"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void saveSuccessfully() throws Exception {
        Long genreId = 3L;

        CreatingGenreDto genreDto = CreatingGenreDto.builder()
                .name("genre_name")
                .build();

        Genre expectedGenre = Genre.builder()
                .id(genreId)
                .name(genreDto.getName())
                .build();

        String content = objectMapper.writeValueAsString(genreDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedGenre, genreRepository.findById(genreId).get()));
    }

    @Test
    public void saveValidationFailedException() throws Exception {
        CreatingGenreDto genreDto = CreatingGenreDto.builder()
                .name("")
                .build();

        String content = objectMapper.writeValueAsString(genreDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Genre's name can't be empty")));
    }

    @Test
    public void updateSuccessfully() throws Exception {
        Long genreId = 1L;

        CreatingGenreDto genreDto = CreatingGenreDto.builder()
                .name("detective renamed")
                .build();

        Genre expectedGenre = Genre.builder()
                .id(genreId)
                .name(genreDto.getName())
                .build();

        String content = objectMapper.writeValueAsString(genreDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/genres/{genreId}", genreId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedGenre, genreRepository.findById(genreId).get()));
    }

    @Test
    public void updateValidationFailedException() throws Exception {
        Long genreId = 1L;

        CreatingGenreDto genreDto = CreatingGenreDto.builder()
                .name("")
                .build();

        String content = objectMapper.writeValueAsString(genreDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/genres/{genreId}", genreId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(x -> assertTrue(x.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", is("Genre's name can't be empty")));
    }

    @Test
    public void deleteById() throws Exception {
        Long genreId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/genres/{genreId}", genreId))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(genreRepository.existsById(genreId)));
    }

    @Test
    public void deleteAll() throws Exception {
        int expectedCountGenres = 0;

        mockMvc.perform(MockMvcRequestBuilders.delete("/genres"))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCountGenres, genreRepository.findAll().size()));
    }
}
